#!/bin/bash

# install kubectl
sudo snap install kubectl --classic

# install docker
curl -sSL https://get.docker.com/ | sh;
usermod -a -G docker vagrant

# install dind
wget https://github.com/kubernetes-sigs/kubeadm-dind-cluster/releases/download/v0.1.0/dind-cluster-v1.13.sh
chmod 755 dind-cluster-v1.13.sh
./dind-cluster-v1.13.sh up

echo 'export PATH="$HOME/.kubeadm-dind-cluster:$PATH"' >> ~/.bashrc
sudo snap install helm --classic
