# launching kubernetes with dind and virtualbox

For more information look [here](https://github.com/kubernetes-sigs/kubeadm-dind-cluster)

## Create VM for development using Vagrantfile

```
vagrant up
```


## Verify kubernetes was successfully created

log into box
```
vagrant ssh
```


check that nodes were created
```
vagrant@ubuntu-xenial:~$ kubectl get nodes
NAME          STATUS    ROLES     AGE       VERSION
kube-master   Ready     master    11m       v1.8.11
kube-node-1   Ready     <none>    10m       v1.8.11
kube-node-2   Ready     <none>    10m       v1.8.11
```



## Start tunnel to kubernetes in the background if you want to view dashboard/api from the host

```
ssh -f -N -i .vagrant/machines/default/virtualbox/private_key -L 8072:localhost:8080  vagrant@10.10.31.10
```

You can then access the dashboard at [http://localhost:8072/api/v1/namespaces/kube-system/services/kubernetes-dashboard:/proxy/#!/node?namespace=default](http://localhost:8072/api/v1/namespaces/kube-system/services/kubernetes-dashboard:/proxy/#!/node?namespace=default)


## Tunnel additional ports to access pod services

since `kubectl port-forward` does not currently allow binding to addresses other than `127.0.0.1`
it is necessary to create another port-forward to access services on your local machine

```sh
function kube-port-forward() {
  service_name=${1}
  local_port=${2}
  remote_port=${3}
  KUBECTL_PATH=${4:-'/usr/bin/kubectl'}

  ssh -t -i .vagrant/machines/default/virtualbox/private_key -L $local_port:127.0.0.1:$local_port  vagrant@10.10.31.10 \
   "$KUBECTL_PATH port-forward $service_name $local_port:$remote_port"
}


function kube-expose() {
  local_port=${1}
  remote_port=${2}
  run_in_bg=${3}

  message="Service listening at localhost:${local_port}"
  ssh_flags="-t"
  fg_command="read"

  if [ ! -z $run_in_bg ]; then
    ssh_flags="-f -N"
    fg_command=""
    echo "${message} in the background. to stop, run this command:"
    echo -e "\nps aux | grep 'ssh ${ssh_flags}' | grep '\\-L ${local_port}' | awk '{print \$2}' | xargs kill -9\n"
  else
    echo "${message}. ^C to stop..."
  fi

  ssh $ssh_flags -i .vagrant/machines/default/virtualbox/private_key -L $local_port:127.0.0.1:$remote_port  vagrant@10.10.31.10 $fg_command
}
```


example:

```sh
$ kube-port-forward kuard 8082 8080 /snap/bin/kubectl
Forwarding from 127.0.0.1:8082 -> 8080
Forwarding from [::1]:8082 -> 8080

# or, if you just need to access a port that has been exposed:

$ kube-expose 8082 8080
Service listening at localhost:8082. ^C to stop...
```
